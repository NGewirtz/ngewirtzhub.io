console.log("Linked")

$('.enter_link').click(function() { 
   $(this).parent().fadeOut(500);
 });

var deck = [];
var deckPics = ["https://upload.wikimedia.org/wikipedia/commons/thumb/3/36/Playing_card_club_A.svg/200px-Playing_card_club_A.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/f/f5/Playing_card_club_2.svg/200px-Playing_card_club_2.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/6/6b/Playing_card_club_3.svg/200px-Playing_card_club_3.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3d/Playing_card_club_4.svg/200px-Playing_card_club_4.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Playing_card_club_5.svg/200px-Playing_card_club_5.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a0/Playing_card_club_6.svg/200px-Playing_card_club_6.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/4/4b/Playing_card_club_7.svg/200px-Playing_card_club_7.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/e/eb/Playing_card_club_8.svg/200px-Playing_card_club_8.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/2/27/Playing_card_club_9.svg/200px-Playing_card_club_9.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/3/3e/Playing_card_club_10.svg/200px-Playing_card_club_10.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b7/Playing_card_club_J.svg/200px-Playing_card_club_J.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Playing_card_club_Q.svg/200px-Playing_card_club_Q.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/2/22/Playing_card_club_K.svg/200px-Playing_card_club_K.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d3/Playing_card_diamond_A.svg/200px-Playing_card_diamond_A.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/5/59/Playing_card_diamond_2.svg/200px-Playing_card_diamond_2.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Playing_card_diamond_3.svg/200px-Playing_card_diamond_3.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Playing_card_diamond_4.svg/200px-Playing_card_diamond_4.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/f/fd/Playing_card_diamond_5.svg/200px-Playing_card_diamond_5.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/8/80/Playing_card_diamond_6.svg/200px-Playing_card_diamond_6.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/e/e6/Playing_card_diamond_7.svg/200px-Playing_card_diamond_7.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Playing_card_diamond_8.svg/200px-Playing_card_diamond_8.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9e/Playing_card_diamond_9.svg/200px-Playing_card_diamond_9.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/3/34/Playing_card_diamond_10.svg/200px-Playing_card_diamond_10.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/a/af/Playing_card_diamond_J.svg/200px-Playing_card_diamond_J.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0b/Playing_card_diamond_Q.svg/200px-Playing_card_diamond_Q.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/7/78/Playing_card_diamond_K.svg/200px-Playing_card_diamond_K.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/5/57/Playing_card_heart_A.svg/200px-Playing_card_heart_A.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d5/Playing_card_heart_2.svg/200px-Playing_card_heart_2.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/b/b6/Playing_card_heart_3.svg/200px-Playing_card_heart_3.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/a/a2/Playing_card_heart_4.svg/200px-Playing_card_heart_4.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Playing_card_heart_5.svg/200px-Playing_card_heart_5.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Playing_card_heart_6.svg/200px-Playing_card_heart_6.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Playing_card_heart_7.svg/200px-Playing_card_heart_7.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Playing_card_heart_8.svg/200px-Playing_card_heart_8.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/5/50/Playing_card_heart_9.svg/200px-Playing_card_heart_9.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/9/98/Playing_card_heart_10.svg/200px-Playing_card_heart_10.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/4/46/Playing_card_heart_J.svg/200px-Playing_card_heart_J.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/7/72/Playing_card_heart_Q.svg/200px-Playing_card_heart_Q.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/d/dc/Playing_card_heart_K.svg/200px-Playing_card_heart_K.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Playing_card_spade_A.svg/200px-Playing_card_spade_A.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/f/f2/Playing_card_spade_2.svg/200px-Playing_card_spade_2.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/5/52/Playing_card_spade_3.svg/200px-Playing_card_spade_3.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/2/2c/Playing_card_spade_4.svg/200px-Playing_card_spade_4.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/9/94/Playing_card_spade_5.svg/200px-Playing_card_spade_5.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/d/d2/Playing_card_spade_6.svg/200px-Playing_card_spade_6.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/6/66/Playing_card_spade_7.svg/200px-Playing_card_spade_7.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/2/21/Playing_card_spade_8.svg/200px-Playing_card_spade_8.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/e/e0/Playing_card_spade_9.svg/200px-Playing_card_spade_9.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/8/87/Playing_card_spade_10.svg/200px-Playing_card_spade_10.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/b/bd/Playing_card_spade_J.svg/200px-Playing_card_spade_J.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/5/51/Playing_card_spade_Q.svg/200px-Playing_card_spade_Q.svg.png",
"https://upload.wikimedia.org/wikipedia/commons/thumb/9/9f/Playing_card_spade_K.svg/200px-Playing_card_spade_K.svg.png"];

var deckBack = "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQ-FSlEqoJyyU4zqB9MHOt3JxtC0K9pTSkqnQD26nBCcKlcCz8sK7tUng";
//var fullDeck =
var bankroll = 1000;
var betAmount;
var dealerWins = false;
var playerWins = false;
var dealerace = false;
var playerace = false;
var split1 = false;
var split2 = false;
var hand1bj = false;
var hand1Bust = false;
var hand2Bust = false;
var hand2bj = false;
var insurance = false;
var dealerBJ = false;

function setUpBoard() {
//$(document).ready(function() {

	var Card = function(suit, rank, image) {
		this.suit = (suit%4)+1;
		this.rank = (rank%13)+1;
		this.image = deckPics[image];
	}

	for (var i=0;i<=51;i++) {
		var card = new Card (i,i,(i));
		deck.push(card);
	}

	$(".play, .hit, .stand, .finish-deal, .finish-deal2, .hit-again, .last-card, .deal, .double, .split, .hand1, .hand2, .insurance").hide();
	$(".play1, .play2, .play3, .play4, .play5, .play7, .play8").show();
	$(".play1, .play2, .play3, .play4, .play5, .play7, .play8, .deal1, .deal2, .deal3, .deal4, .deal5").html("");
	$(".dealer-has").html("Dealer");
	$(".player-has").html("Player");
	$(".winner").css("font-size", "50px");	
	dealerHas = 0;
	playerHas = 0;
	dealerWins=false;
	playerWins=false;
	dealerace = false;
	playerace = false;
	split1 = false;
	split2 = false;
	hand1bj = false;
	hand1Bust = false;
	hand2Bust = false;
	hand2bj = false;
	insurance = false;
	dealerBJ = false;
};


setUpBoard()


function checkWin() {
	if (dealerWins===true) {
		$("#modal").fadeIn(1500);
		bankroll -= betAmount;
		$("p").html("Your Available Bankroll is $"+bankroll);
		console.log(bankroll)
		$(".winner").html("Dealer Won!");
	}
};


function checkPlayerWin () {
	if (playerWins===true){
		$("#modal").fadeIn(1500);
		bankroll += betAmount;
		$("p").html("Your Available Bankroll is $"+bankroll);
		$(".winner").html("Player Won!");
		console.log(bankroll)
	}
}


function checkPush () {
	if (playerHas===dealerHas) {
		$("#modal").fadeIn(1500);	
		$(".winner").html("Its A Push!");
	}
}


function otherWin() {
	if(split2===false&&dealerHas>=17) {
		checkPush();
		if (dealerHas !== playerHas) {
		(dealerHas>playerHas) ? dealerWins=true:playerWins=true;
		}
	}
}


function checkHand1() {
	if (split1===true) {
		$("#modal").fadeOut(1500)
		$(".stand, .hit, .hit-again, .last-card").hide();
		$(".hand2").show();
		$("#close").hide();
		playerWins=false;
		dealerWins=false;
	}
}

function checkHand2() {
	if (split2===true) {
		$("#modal").fadeOut(1500)
		$(".stand, .hit, .hit-again, .last-card, .hand2").hide();
		$(".finish-deal2").show();
		$("#close").hide();
		playerWins=false;
		dealerWins=false;	
	}
}

$(".deal6").html("<img src="+deckBack+" height='200px' width='125px'>" );
$(".deal6").css("float", "left");

$(".play6").html("<img src="+deckBack+" height='200px' width='125px'>" );
$(".play6").css("float", "left");



$(".deal").click(function() {

	var randomCard = Math.floor(Math.random()*deck.length);
	$(".deal1").html("<img src="+deck[randomCard].image+">");
	$(".deal1").css("float", "right");
	card1Value = deck[randomCard].rank;
	deck.splice(randomCard, 1);
	//console.log(bankroll)

	if (card1Value ===1) {
			insurance = confirm("Would You Like Insurance? Wager Half Your Bet: Pays 2-1!")
			if (insurance === true) {
				bankroll -= betAmount*.5;
			}
	}

	randomCard2 = Math.floor(Math.random()*deck.length);
	faceDownCard = "<img src="+deck[randomCard2].image+">";
	$(".deal2").html("<img src="+deckBack+" height='200px' width='125px'>");
	$(".deal2").css("float", "right");
	card2Value = deck[randomCard2].rank;
	deck.splice(randomCard2, 1);

	if (card1Value>=10){
		card1Value=10;
	}

	if (card2Value>=10){
		card2Value=10;
	}

	if (card1Value === 1 && card2Value === 10){
		$(".deal").hide();
		$(".deal2").html(faceDownCard);
		$(".dealer-has").html("Dealer Has: BLACKJACK!");
		//dealerWins = true;
		dealerBJ = true;
		if (insurance===true) {
			bankroll+=betAmount*1.5;
		}
	}else if (card2Value === 1 && card1Value === 10){
		$(".deal").hide();
		$(".deal2").html(faceDownCard);
		$(".dealer-has").html("Dealer Has: BLACKJACK!");
		//dealerWins = true;
		dealerBJ = true;
	}

	dealerHas = (card1Value+card2Value);

	if (card1Value === 1) {
		dealerHas +=10;
		dealerace = true;
	}else if (card2Value === 1) {
		dealerHas +=10;
		dealerace = true;
	}

	$(".dealer-has").html("Player Its Your Turn!");
	$(".deal").hide();
	$(".play").show();

	checkWin();
});
	


$(".play").click(function() {
	var randomCard = Math.floor(Math.random()*deck.length);
	$(".play1").html("<img src="+deck[randomCard].image+">");
	$(".play1").css("float", "right");
	play1Value = deck[randomCard].rank;
	deck.splice(randomCard, 1);

	var randomCard2 = Math.floor(Math.random()*deck.length);
	$(".play2").html("<img src="+deck[randomCard2].image+">");
	$(".play2").css("float", "right");
	var play2Value = deck[randomCard2].rank;
	deck.splice(randomCard2, 1);

	if (play1Value===play2Value&&(betAmount*2)<=bankroll){
		$(".split").show()
	}

	if (play1Value>=10){
		play1Value=10;
	}

	if (play2Value>=10){
		play2Value=10;
	}

	playerHas = (play1Value+play2Value);

	if (play1Value === 1) {
		playerHas +=10;
		playerace = true;
	}else if (play2Value === 1) {
		playerHas +=10;
		playerace = true;
	}

	$(".player-has").html("Player Has: " + playerHas);

	if (play1Value === 1 && play2Value === 10){
		$(".deal").hide();
		$(".deal2").html(faceDownCard+">");
		$(".player-has").html("Player Has: BLACKJACK!");
		if (dealerBJ===false) {
			playerWins = true;
			betAmount = betAmount*1.5;
			checkPlayerWin ();
		}else if (dealerBJ===true){
			playerHas=dealerHas;
			checkPush();
		}
	}else if (play2Value === 1 && play1Value === 10){
		$(".player-has").html("Player Has: BLACKJACK!");
		$(".deal2").html(faceDownCard);
		if (dealerBJ===false) {
			playerWins = true;
			betAmount = betAmount*1.5;
			checkPlayerWin ();
		}else if (dealerBJ===true){
			playerHas===dealerHas;
			checkPush();
		}
	}

	if(dealerBJ===true){
		dealerWins=true;
		checkWin();
	}

	$(".play, .insurance").hide();
	$(".hit").show();
	$(".stand").show();

	if ((betAmount*2)<=bankroll){
		$(".double").show();
	}
});



$(".hit, .hand1").click(function() {
	var randomCard = Math.floor(Math.random()*deck.length);
	$(".play3").html("<img src="+deck[randomCard].image+">");
	$(".play3").css("float", "right");
	var play3Value = deck[randomCard].rank

	if (play3Value>=10){
		play3Value=10;
	}

	playerHas += play3Value;

	if (play3Value === 1) {
		playerHas +=10;
		playerace = true;
	}
	if (playerHas>21 && playerace===true) {
		playerHas -=10;
		playerace = false;
	}
	deck.splice(randomCard, 1);

	$(".player-has").html("Player Has: " + playerHas);

	if (split1===true){
		hand1Value+=play3Value
		if (play3Value === 1) {
			hand1Value +=10;
			playerace = true;
		}
		if (hand1Value>21 && playerace===true) {
			hand1Value -=10;
			playerace = false;
		}
		$(".player-has").html("Player Hand 1: " + hand1Value);
		$(".hand1").hide();
		$(".stand, .play3").show();

		if (hand1Value>21) {
			$(".player-has").html("Player Has Busted!");
			dealerWins = true;
			hand1Bust=true;
			$(".hit, .stand").hide();
			$(".hand2").show();
			checkWin();
			checkHand1();
		}else if (hand1Value===21){
			$(".player-has").html("Player Has: BLACKJACK!");
			//hand1bj=true;
			$(".hit, .stand").hide();
			$(".hand2").show();
		}else {
			$(".hit").hide();
			$(".hit-again").show();
			$(".double").hide();
			$(".split").hide()
		}
	}else if (split2===true){
			$(".hand2").hide();
			hand2Value+=play3Value
			if (play3Value === 1) {
				hand2Value +=10;
				playerace = true;
			}
			if (hand2Value>21 && playerace===true) {
				hand2Value -=10;
				playerace = false;
			}
			$(".player-has").html("Player Hand 2: " + hand2Value);
			$(".hand1").hide();
			$(".stand, .play3").show();

			if (hand2Value>21) {
				$(".player-has").html("Player Has Busted!");
				dealerWins = true;
				hand2Bust=true;
				$(".hit, .stand").hide();
				$(".finish-deal").show();
				checkWin();
				checkHand2();
			}else if (hand2Value===21){
				$(".player-has").html("Player Has: BLACKJACK!");
				//hand2bj=true;
				$(".hit, .stand").hide();
				$(".stand").show();
			}else {
				$(".hit").hide();
				$(".hit-again").show();
				$(".double").hide();
				$(".split").hide()
			}
	}else if (playerHas>21) {
		$(".player-has").html("Player Has Busted!");
		dealerWins = true;
		checkWin();
	}else if (playerHas===21){
		$(".player-has").html("Player Has: BLACKJACK!");
		$(".hit, .double, .split").hide();
	}else{
		$(".hit, .double, .split").hide();
		$(".hit-again").show();
	}

});



$(".double").click(function() {
	var randomCard = Math.floor(Math.random()*deck.length);
	$(".play3").html("<img src="+deck[randomCard].image+">");
	$(".play3").css("float", "right");
	var play3Value = deck[randomCard].rank

	if (play3Value>=10){
		play3Value=10;
	}

	playerHas += play3Value;

	if (play3Value === 1) {
		playerHas +=10;
		playerace = true;
	}

	if (playerHas>21 && playerace===true) {
		playerHas -=10;
		playerace = false;
	}

	deck.splice(randomCard, 1);
	betAmount=betAmount*2

	$(".player-has").html("Player Has: " + playerHas);	

	if (playerHas>21) {
		$(".player-has").html("Player Has Busted!");
		dealerWins = true;
	} else if (playerHas===21){
		$(".player-has").html("Player Has: BLACKJACK!");
	}

	otherWin();
	checkPlayerWin();
	checkWin();

	$(".hit, .stand, .double, .split, .hit-again, .last-card").hide();
	$(".finish-deal").show();
	$(".dealer-has").html("Dealer has "+ dealerHas);
	$(".deal2").html(faceDownCard);
});


$(".hit-again").click(function() {
	var randomCard = Math.floor(Math.random()*deck.length);
	$(".play4").html("<img src="+deck[randomCard].image+">");
	$(".play4").css("float", "right");
	var play4Value = deck[randomCard].rank;

	if (play4Value>=10){
		play4Value=10;
	}

	playerHas += play4Value;

	if (play4Value === 1) {
		playerHas +=10;
		playerace = true;
	}
	if (playerHas>21 && playerace===true) {
		playerHas -=10;
		playerace = false;
	}
	deck.splice(randomCard, 1);

	$(".player-has").html("Player Has: " + playerHas);

	if (split1===true){
		hand1Value+=play4Value
		if (play4Value === 1) {
			hand1Value +=10;
			playerace = true;
		}
		if (hand1Value>21 && playerace===true) {
			hand1Value -=10;
			playerace = false;
		}
		$(".player-has").html("Player Hand 1: " + hand1Value);
		$(".hit-again").hide();
		$(".play4").show();

		if (hand1Value>21) {
			$(".player-has").html("Player Has Busted!");
			dealerWins = true;
			hand1Bust=true;
			$(".hit-again, .stand").hide();
			$(".hand2").show();
			checkWin();
			checkHand1();
		}else if (hand1Value===21){
			$(".player-has").html("Player Has: BLACKJACK!");
			//hand1bj=true;
			$(".hit-again, .stand").hide();
			$(".hand2").show();
		}else {
			$(".last-card").show();
		}
	}else if (split2===true){
		hand2Value+=play4Value
		if (play4Value === 1) {
			hand2Value +=10;
			playerace = true;
		}
		if (hand2Value>21 && playerace===true) {
			hand2Value -=10;
			playerace = false;
		}
		$(".player-has").html("Player Hand 2: " + hand2Value);
		$(".hit-again").hide();
		$(".play4").show();

		if (hand2Value>21) {
			$(".player-has").html("Player Has Busted!");
			dealerWins = true;
			//hand2bj=true;
			$(".hit-again, .stand").hide();
			$(".finish-deal").show();
			checkWin();
			checkHand2();
		}else if (hand2Value===21){
			$(".player-has").html("Player Has: BLACKJACK!");
			//hand2bj=true;
			$(".hit-again, .stand").hide();
			$(".stand").show();
		}else {
			$(".last-card").show();
		}
	}else if (playerHas>21) {
		$(".player-has").html("Player Has Busted!");
		dealerWins = true;
		checkWin();
	}else if (playerHas===21){
		$(".player-has").html("Player Has: BLACKJACK!");
		$(".hit-again").hide();
	}else{
		$(".hit-again").hide();
		$(".last-card").show();
	}
});


$(".last-card").click(function() {
	var randomCard = Math.floor(Math.random()*deck.length);
	$(".play5").html("<img src="+deck[randomCard].image+">");
	$(".play5").css("float", "right");
	var play5Value = deck[randomCard].rank;

	if (play5Value>=10){
		play5Value=10;
	}

	playerHas += play5Value;

	if (play5Value === 1) {
		playerHas +=10;
		playerace = true;
	}
	if (playerHas>21 && playerace===true) {
		playerHas -=10;
		playerace = false;
	}
	deck.splice(randomCard, 1);

	$(".player-has").html("Player Has: " + playerHas);

	if (split1===true){
		hand1Value+=play5Value
		if (play5Value === 1) {
			hand1Value +=10;
			playerace = true;
		}
		if (hand1Value>21 && playerace===true) {
			hand1Value -=10;
			playerace = false;
		}
		$(".player-has").html("Player Hand 1: " + hand1Value);
		$(".hit, .hit-again").hide();
		$(".play5").show();

		if (hand1Value>21) {
			$(".player-has").html("Player Has Busted!");
			dealerWins = true;
			hand1Bust=true;
			$(".last-card, .stand").hide();
			$(".hand2").show();
			checkWin();
			checkHand1();
		}else if (hand1Value===21){
			$(".player-has").html("Player Has: BLACKJACK!");
			//hand1bj=true;
			$(".last-card, .stand").hide();
			$(".hand2").show();
		}else {
			$(".hand2").show();
		}
	}else if (split2===true){
		hand2Value+=play5Value
		if (play5Value === 1) {
			hand2Value +=10;
			playerace = true;
		}
		if (hand2Value>21 && playerace===true) {
			hand2Value -=10;
			playerace = false;
		}
		$(".player-has").html("Player Hand 2: " + hand2Value);
		$(".hit, .hit-again").hide();
		$(".play5").show();

		if (hand2Value>21) {
			$(".player-has").html("Player Has Busted!");
			dealerWins = true;
			hand2Bust=true;
			$(".last-card, .stand").hide();
			$(".finish-deal2").show();
			checkWin();
			checkHand2();
		}else if (hand2Value===21){
			$(".player-has").html("Player Has: BLACKJACK!");
			//hand2bj=true;
			$(".last-card, .stand").hide();
			$(".finish-deal2").show();
		}else {
			$(".finish-deal2").show();
		}	
	}else if (playerHas>21) {
		$(".player-has").html("Player Has Busted!");
		dealerWins = true;
		checkWin();
	}else if (playerHas===21){
		$(".player-has").html("Player Has: BLACKJACK!");
		$(".last-card").hide();
	}else{
		$(".last-card").hide();
		$(".deal").show();
	}
});


$(".stand, .finish-deal2").click(function(){
	if (split1===true){
		$(".stand, .hit, .hit-again, .last-card").hide();
		$(".hand2").show()
	} else {
		otherWin();
		$(".hit").hide();
		$(".stand").hide();
		$(".hit-again").hide();
		$(".last-card, .finish-deal2").hide();
		$(".finish-deal").show();
		$(".dealer-has").html("Dealer has "+ dealerHas);
		$(".deal2").html(faceDownCard);
		$(".double").hide();
		$(".split").hide();
	}
	checkWin();
	checkPlayerWin();
});



$(".finish-deal").click(function(){
	$("#close").show();
	if (dealerHas<17) {
		var randomCard3 = Math.floor(Math.random()*deck.length);
		$(".deal3").html("<img src="+deck[randomCard3].image+">");
		$(".deal3").css("float", "right")
		var card3Value = deck[randomCard3].rank

		if (card3Value>=10){
			card3Value=10;
		}

		dealerHas += card3Value

		if (card3Value === 1) {
			dealerHas +=10;
			dealerace = true;
		}
		if (dealerHas>21 && dealerace===true) {
			dealerHas -=10;
			dealerace = false;
		}
			
		deck.splice(randomCard3, 1)
		$(".dealer-has").html("Dealer Has: " + dealerHas);
	}

	if (dealerHas>21) {
		$(".dealer-has").html("Dealer Has Busted!");
		playerWins=true
	}else if (dealerHas===21){
		$(".dealer-has").html("Dealer Has: BLACKJACK!");
		//dealerWins=true
	}


	if (dealerHas<17) {
		var randomCard4 = Math.floor(Math.random()*deck.length);
		$(".deal4").html("<img src="+deck[randomCard4].image+">");
		$(".deal4").css("float", "right")
		var card4Value = deck[randomCard4].rank;

		if (card4Value>=10){
			card4Value=10
		}

		dealerHas += card4Value;

		if (card4Value === 1) {
			dealerHas +=10;
			dealerace = true;
		}

		if (dealerHas>21 && dealerace===true) {
			dealerHas -=10;
			dealerace = false;
		}

		deck.splice(randomCard4, 1)
		$(".dealer-has").html("Dealer Has: " + dealerHas);
	}

	if (dealerHas>21) {
		$(".dealer-has").html("Dealer Has Busted!");
		playerWins = true;
	}else if (dealerHas===21){
		$(".dealer-has").html("Dealer Has: BLACKJACK!");
		//dealerWins = true;
	}


	if (dealerHas<17) {
		var randomCard5 = Math.floor(Math.random()*deck.length);
		$(".deal5").html("<img src="+deck[randomCard5].image+">");
		$(".deal5").css("float", "right")
		var card5Value = deck[randomCard5].rank;

		if (card5Value>=10){
			card5Value=10;
		}

		dealerHas += card5Value;

		if (card5Value === 1) {
			dealerHas +=10;
			dealerace = true;
		}

		if (dealerHas>21 && dealerace===true) {
			dealerHas -=10;
			dealerace = false;
		}

		deck.splice(randomCard5, 1);
		$(".dealer-has").html("Dealer Has: " + dealerHas);
	}



	if (split2===true) {
		if (hand1Bust===true) {
			var hand1win = "Hand 1: Dealer Won! ";
		} else if (hand1bj===true&&dealerBJ===false) {
			hand1win = "Hand 1: Player Won! ";
		}else if (hand1bj===true&&dealerBJ===true) {
			hand1win = "Hand 1: Push: ";
		}else if (dealerHas<22) {
			hand1win = (dealerHas>hand1Value) ? "Hand1: Dealer Won! " : "Hand 1: Player Won! ";
			if (dealerHas<hand1Value) {
				bankroll+=betAmount;
			}else if (hand1Value<dealerHas){
				bankroll-=betAmount;
			} else if (hand1Value===dealerHas){
				hand1win = "Hand1: Push: "
			} 
		}
			if (dealerHas>21 && hand1Bust===false){
				hand1win = "Hand1: Player Won! ";
				bankroll += betAmount;
			}
		if (hand2Bust===true) {
			var hand2win = "Hand 2: Dealer Won!";
		}else if (hand2bj===true&&dealerBJ===false) {
			hand2win = "Hand 2: Player Won!";
		}else if (hand2bj===true&&dealerBJ===true) {
			hand2win = "Hand 2: Push: ";
		}else if (dealerHas<22) {
			hand2win = (dealerHas>hand2Value) ? "Hand2: Dealer Won! " : "Hand 2: Player Won! ";
			if (dealerHas<hand2Value) {
				bankroll+=betAmount;
			}else if (hand2Value<dealerHas){
				bankroll-=betAmount;
			} else if (hand2Value===dealerHas){
				hand2win = "Hand2: Push! "
			}
		} 
			if (dealerHas>21 && hand2Bust===false){
				hand2win = "Hand2: Player Won! "
				bankroll += betAmount;
			}

	$(".winner").css("font-size", "30px");	
	$(".winner").html(hand1win+hand2win);
	$("p").html("Your Available Bankroll is $"+bankroll);
	$("#modal").fadeIn(1500);	
	}else if (split2===false){
		if (dealerHas>21) {
			$(".dealer-has").html("Dealer Has Busted!");
			playerWins = true;
		} else if (dealerHas===21&&playerHas!==21){
			$(".dealer-has").html("Dealer Has: BLACKJACK!");
			dealerWins = true;
		} else if (dealerHas>playerHas) {
			dealerWins = true;
		} else if (dealerHas<playerHas){
			playerWins = true;
		}
	checkWin();
	checkPlayerWin();
	checkPush();
	}
});


$(".split").click(function() {
	split1 = true
	hand1Value = play1Value;

	if (hand1Value === 1) {
		hand1Value +=10;
		playerace = true;
	}

	$(".play2, .play3, .play4, .play5, .play8, .split, .double").hide();

	var randomCard = Math.floor(Math.random()*deck.length);
	$(".play7").html("<img src="+deck[randomCard].image+">");
	$(".play7").css("float", "right");
	play7Value = deck[randomCard].rank;
	deck.splice(randomCard, 1);
	$(".play7").show();

	if (play7Value>=10){
		play7Value=10;
	}


	hand1Value += play7Value;

	if (play7Value === 1) {
		hand1Value +=10;
		playerace = true;
	}
	if (hand1Value>21 && playerace===true) {
		hand1Value -=10;
		playerace = false;
	}

		
	$(".player-has").html("Player Hand 1: " + hand1Value);

	if (hand1Value===21){
		$(".player-has").html("Player Has: BLACKJACK!");
		hand1bj=true;
		if (dealerBJ===false) {
			playerWins = true;
			bankroll += betAmount*1.5;
			checkPlayerWin();
			checkHand1();
		}
	}
});

$(".hand2").click(function() {
	split1 = false;
	split2 = true;
	playerace = false;
	hand2Value = play1Value;

	if (hand2Value === 1) {
		hand2Value +=10;
		playerace = true;
	}

	$(".hit-again, .last-card, .hand2, .play1, .play2, .play3, .play4, .play5, .play7").hide();
	$(".play2, .play8, #close").show();
	$(".hit, .stand").show()

	var randomCard = Math.floor(Math.random()*deck.length);
	$(".play8").html("<img src="+deck[randomCard].image+">");
	$(".play8").css("float", "right");
	play8Value = deck[randomCard].rank;
	deck.splice(randomCard, 1);

	if (play8Value>=10){
		play8Value=10;
	}


	hand2Value += play8Value;

	if (play8Value === 1) {
		hand2Value +=10;
		playerace = true;
	}

	if (hand2Value>21 && playerace===true) {
		hand2Value -=10;
		playerace = false;
	}

	$(".player-has").html("Player Hand 2: " + hand2Value);

	if (hand2Value===21){
		$(".player-has").html("Player Has: BLACKJACK!");
		hand2bj=true
		if (dealerBJ===false){
			playerWins = true;
			bankroll += betAmount*1.5;
			checkPlayerWin();
			checkHand2();
		}
	}
});


$(".enter_link").on('click', function() {
	$("#modal").toggle();
	$("p").html("Your Available Bankroll is $"+bankroll);
});


$("#close").on('click', function(){
	betAmount=parseInt($(".bet-amount").val());
		if (bankroll === 0) {
			var reset = confirm("You've Gone Bust! Hit OK To Reset Your Bankroll!");
			if (reset === true) {
				bankroll = 1000;
			}
		}else if(betAmount > bankroll){
			console.log(betAmount);
			alert("Please bet within your limits!");
		} else if (betAmount<0) {
			alert("Nice Try!");
		} 

	if(betAmount>=0 && betAmount<=bankroll){
		//console.log(betAmount);
		setUpBoard();
		$(".deal").show();
		$("#modal").toggle();
	}
});


